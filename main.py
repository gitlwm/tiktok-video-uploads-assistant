import os
import subprocess
import time

def main():
    for dirs in os.listdir(r'./'):
        if dirs == 'taiyuan':
            for d in os.listdir(dirs):
                popen('worker.py %s rm' % os.path.join(dirs, d))
                time.sleep(1)

def popen(cmd):
    p = subprocess.Popen(cmd, bufsize=10000, shell=True, stdout=subprocess.PIPE)
    for i in iter(p.stdout.readline, ''):
        if len(i) < 1:
            break
        print(i.decode('gbk').strip())

if __name__ == '__main__':
    main()
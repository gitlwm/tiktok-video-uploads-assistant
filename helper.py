# -*- coding: utf-8 -*-
import os
import requests
import json
import time
from hashlib import md5

#todo: 生成待发布坐标， 存储所有待发布坐标
#todo: 记录发布点 ，已发布标识

#视频内容索引 -> 待发布队列

class Helper(object):
    def __init__(self):
        pass

    def location(self, keywords='太原'):
        params = {
            's': 'rsv3',
            'children': '',
            'key': '6e79f6d236e295632f21b385e363b6e8',
            'offset': 1,
            'page': 1,
            'extensions': 'all',
            'city': '110000',
            'language': 'zh_cn',
            'platform': 'JS',
            'logversion': '2.0',
            'appname': 'https://lbs.amap.com/tools/picker',
            'csid': '7235DF14-9861-493C-8ABB-066F71AC0BFC',
            'sdkversion': '1.4.15',
            'keywords': keywords
        }
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, sdch, br",
            "Accept-Language": "zh-CN,zh;q=0.8",
            "Connection": "keep-alive",
            "Cookie": "key=8325164e247e15eea68b59e89200988b; guid=f631-e4a9-c4a0-275d; UM_distinctid=16e68043e7113e-0b9c86ad90cb11-4d045769-1fa400-16e68043e728d",
            "Host": "restapi.amap.com",
            "Referer": "https://lbs.amap.com/console/show/picker",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0"
        }

        req = requests.get('https://restapi.amap.com/v3/place/text', params=params, headers=headers)
        res = req.json()
        adcode = res['pois'][0]['adcode']
        location = res['pois'][0]['location'].split(',')
        return adcode, location[0], location[1]

    def dumppoi(self, cityName):
        adcode, lng, lat = self.location(cityName)
        res = {}
        # 每1公里1个点，经纬度移动距离大约5:4
        # 向东99公里
        for i in range(1, 100):
            lngstr = '%.6f' % (float(lng) + 0.01 * i)
            for j in range(1, 100):
                latstr = '%.6f' % (float(lat) + 0.008 * j)
                hexdig = md5((lngstr+','+latstr).encode('utf-8')).hexdigest()
                res[hexdig] = [lngstr, latstr]

        # 向南99公里
        for i in range(1, 100):
            lngstr = '%.6f' % (float(lng) + 0.01 * i)
            for j in range(1, 100):
                latstr = '%.6f' % (float(lat) - 0.008 * j)
                hexdig = md5((lngstr+','+latstr).encode('utf-8')).hexdigest()
                res[hexdig] = [lngstr, latstr]

        # 向西99公里
        for i in range(1, 100):
            lngstr = '%.6f' % (float(lng) - 0.01 * i)
            for j in range(1, 100):
                latstr = '%.6f' % (float(lat) - 0.008 * j)
                hexdig = md5((lngstr+','+latstr).encode('utf-8')).hexdigest()
                res[hexdig] = [lngstr, latstr]

        # 向北99公里
        for i in range(1, 100):
            lngstr = '%.6f' % (float(lng) - 0.01 * i)
            for j in range(1, 100):
                latstr = '%.6f' % (float(lat) + 0.008 * j)
                hexdig = md5((lngstr+','+latstr).encode('utf-8')).hexdigest()
                res[hexdig] = [lngstr, latstr, 0]

        with open('./cache/%s.json' % adcode, 'w', encoding='utf-8') as f:
            json.dump(res, f)
        return True

    def cityslist(self):
        res = {}
        with open('./citys.json', 'r', encoding='utf-8') as f:
            res = json.load(f)
        return res

    def test(self):
        def convertTime(fmtime):
            return time.mktime(time.strptime(fmtime, "%Y-%m-%d %H:%M:%S"))
        endTime = int(convertTime(time.strftime("%Y-%m-%d 23:59:59", time.localtime(time.time()))) * 1000)
        startTime = endTime - 7*86400*1000 + 1000
        print(endTime, startTime)
        #print(md5('112.344232,37.245454'.encode('utf-8')).hexdigest())

if __name__ == '__main__':
    hp = Helper()
    hp.test()
    #hp.dumppoi('太原')